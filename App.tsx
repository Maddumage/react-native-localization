import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, Button} from 'react-native';
import * as Localization from 'react-native-localize';

import I18n, {LocalizationContext} from './utils/i18n';

const MyComponent: React.FC = () => {
  const {t, locale, setLocale} = React.useContext(LocalizationContext);
  return (
    <View style={styles.sectionContainer}>
      <Text style={styles.sectionTitle}>
        Current locale: {locale}.{' '}
        {locale !== 'en' && locale !== 'no'
          ? 'Translations will fall back to "en" because none available'
          : null}
      </Text>
      <Text>{t('hello', {someValue: Date.now()})}</Text>
      {locale === 'en' ? (
        <Button title="Switch to Norvigean" onPress={() => setLocale('no')} />
      ) : (
        <Button title="Switch to English" onPress={() => setLocale('en')} />
      )}
    </View>
  );
};

const Com: React.FC = () => {
  const {t} = React.useContext(LocalizationContext);
  return (
    <View style={styles.sectionContainer}>
      <Text>{t('hello', {someValue: Date.now()})}</Text>
    </View>
  );
};

const App: React.FC = () => {
  const [locale, setLocale] = React.useState<any>();
  const localizationContext = React.useMemo(
    () => ({
      t: (scope: any, options: any) => I18n.t(scope, {locale, ...options}),
      locale,
      setLocale,
    }),
    [locale],
  );
  return (
    <LocalizationContext.Provider value={localizationContext}>
      <SafeAreaView>
        <View style={styles.sectionContainer}>
          <Text style={styles.sectionTitle}>Learn More</Text>
          <Text style={styles.sectionDescription}>
            Read the docs to discover what to do next:
          </Text>
          <Com />
          <MyComponent />
        </View>
      </SafeAreaView>
    </LocalizationContext.Provider>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#FFF',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: '#fff',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: '#000',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: '#aeaeea',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: '#aeaeea',
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
