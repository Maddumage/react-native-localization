import * as React from 'react';
import I18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';

import en from './locales/en';
import no from './locales/no';

const locales = RNLocalize.getLocales();

export const LocalizationContext = React.createContext({});

if (Array.isArray(locales)) {
  I18n.locale = locales[0].languageTag;
}

I18n.fallbacks = true;
I18n.translations = {
  en,
  no,
};

export default I18n;
